from flask import Flask, request, render_template_string

app = Flask(__name__)

# Page HTML template avec formulaire de recherche
PAGE_HTML = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../static/style.css">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>

</head>
<body>
    <header class="header">
        <a href="#" class="logo">Thunder Surge</a>

        <box-icon name='menu' id="menu-icon"></box-icon>


        <nav class="navbar">
            <a href="#home" class="active">Acueil</a>
            <a href="#search">Recherches</a>
            <a href="#jeux">Jeux</a>
            <a href="#profil">Profil</a>
        </nav>
    </header>

    <section class="home" id="home">
        <div class="home-content">
            <h1>Bienvenue</h1>
            <div class="text-animator">
                <h3>à l'accueil</h3>
            </div>
            <p>Ici c'est l'accueil, endroit où vous pouvez choisir la catégorie que vous voulez accéder.</p><p>C'est aussi un endroit pour se reposer.</p>
        </div>
        <div class="home-imgHover"></div>
    </section>








    <section class="search" id="search">
        <div class="search-content">
            <h1>Faites vos recherches</h1>

            <form method="POST">
                <input type="text" name="recherche" placeholder="Recherche...">
                <input type="submit" value="Rechercher">
            </form>
            {% if section %}
                <div>
                    <h2>Section trouvée : {{ section }}</h2>
                    <p>Contenu de la section {{ section }}...</p>
                </div>
            {% endif %}
            
            <div class="blog-box">
                <div class="blog-content">
                    <div class="content">
                        <h3>Alimentation</h3>
                        <p></p>
                    </div>
                </div>

                <div class="blog-box">
                    <div class="blog-content">
                        <div class="content">
                            <h3>Back</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                        </div>
                    </div>
            
                    <div class="blog-box">
                        <div class="blog-content">
                            <div class="content">
                                <h3>Shoulder</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                            </div>
                        </div>

                        <div class="blog-box">
                            <div class="blog-content">
                                <div class="content">
                                    <h3>Triceps</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                </div>
                            </div>

                            <div class="blog-box">
                                <div class="blog-content">
                                    <div class="content">
                                        <h3>Biceps</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                    </div>
                                </div>

                                <div class="blog-box">
                                    <div class="blog-content">
                                        <div class="content">
                                            <h3>abdominal</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                        </div>
                                    </div>

                                    <div class="blog-box">
                                        <div class="blog-content">
                                            <div class="content">
                                                <h3>legs</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                            </div>
                                        </div>
                                        
                                        <div class="blog-box">
                                            <div class="blog-content">
                                                <div class="content">
                                                    <h3>traps</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                                </div>
                                            </div>

                                            <div class="blog-box">
                                                <div class="blog-content">
                                                    <div class="content">
                                                        <h3>forearm</h3>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                                    </div>
                                                </div>

                                                <div class="blog-box">
                                                    <div class="blog-content">
                                                        <div class="content">
                                                            <h3>Cardio</h3>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit veniam nesciunt neque accusantium officiis ab expedita esse cupiditate, voluptates, autem impedit velit, sapiente quaerat? Rem dicta alias nisi voluptatibus blanditiis minima possimus.</p>
                                                        </div>
                                                    </div>
            </div>
        </div>
    </section>

    <section class="jeux" id="jeux">
        <div class="jeux-content">
            <h1>Tentez d'arracher la victoire</h1>
            <p></p>
        </div>
    </section>

    <section class="profil" id="profil">
        <div class="profil-content">
            <h1>Votre profil</h1>
            <p></p>
           <!-- Dans la section de calcul de l'IMC -->
            <form action="/" method="POST" class="search-text">
                <input type="text" name="poids" class="input" placeholder="Votre poids en kg">
                <input type="text" name="taille" class="input" placeholder="Votre taille en mètres">
                <input type="submit" value="Calculer IMC">
            </form>
            <!-- Juste après le formulaire dans votre section de calcul de l'IMC -->
            <div id="resultat-imc">
                {% if resultat %}
                    <p>{{ resultat }}</p>
                {% endif %}
            </div>


            
        </div>
    </section>


    <script src="#"></script>
</body>
</html>
"""

@app.route('/', methods=['GET', 'POST'])
def recherche():
    section_recherchee = None
    if request.method == 'POST':
        recherche = request.form.get('recherche', '').lower()
        # Ici, tu peux vérifier si le terme recherché correspond à une section spécifique
        # Par exemple, si tu as une base de données ou une liste de sections valides
        sections_valides = ['alimentation', 'sport', 'technologie']
        if recherche in sections_valides:
            section_recherchee = recherche
    return render_template_string(PAGE_HTML, section=section_recherchee)

if __name__ == '__main__':
    app.run(debug=True)