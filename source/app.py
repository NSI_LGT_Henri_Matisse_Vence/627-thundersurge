from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():
    resultat = ""
    if request.method == 'POST':
        try:
            poids = float(request.form['poids'])
            # Assurez-vous que la taille est convertie en mètres si elle est entrée en centimètres.
            taille = float(request.form['taille']) / 100  # Convertir en mètres si nécessaire
            imc = poids / (taille ** 2)
            
            if imc < 18.5:
                classification = 'Vous êtes en sous-poids.'
            elif 18.5 <= imc < 25:
                classification = 'Votre poids est normal.'
            elif 25 <= imc < 30:
                classification = 'Vous êtes en surpoids.'
            elif imc >= 30:
                classification = 'Vous êtes en obésité.'
            resultat = f'Votre IMC est {imc:.2f}. {classification}'
        except ValueError:
            resultat = "Veuillez entrer des valeurs valides pour le poids et la taille."
    
    # Passez le résultat du calcul (ou une chaîne vide si rien n'a été calculé) au template
    return render_template('index.html', resultat=resultat)

if __name__ == '__main__':
    app.run(debug=True)