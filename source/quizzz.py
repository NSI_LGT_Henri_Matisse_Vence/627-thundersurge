from flask import Flask, request, render_template_string

app = Flask(__name__)

# Définition des questions et des réponses
questions = [
    "Quel est l'un des principaux avantages de l'entraînement en force (musculation) ?<br>(a) Perdre du poids<br>(b) Renforcer et développer les muscles<br>(c) Améliorer la flexibilité<br>",
    "Quel est le terme utilisé pour décrire le nombre de répétitions d'un exercice effectuées en une série ?<br>(a) Tapis de yoga<br>(b) Haltères<br>(c) Vélo stationnaire<br>",
    "Quel type d'exercice est le soulevé de terre ?<br>(a) Les bras<br>(b) Le dos<br>(c) La poitrine<br>",
    "Quel est le rôle principal des protéines dans la construction musculaire ?<br>(a) Fournir de l'énergie<br>(b) Aider à la réparation et à la croissance musculaire<br>(c) Améliorer la flexibilité<br>",
    "Qu'est-ce que le temps sous tension (TUT) dans le contexte de l'entraînement en musculation ?<br>(a) La durée pendant laquelle un muscle reste contracté<br>(b) Le temps de repos entre les séries<br>(c) La vitesse à laquelle les exercices sont effectués<br>",
    "Qu'est-ce que la périodisation dans le contexte de l'entraînement en musculation ?<br>(a) Une technique de respiration<br>(b) La planification variée de l'entraînement sur plusieurs cycles<br>(c) Une méthode de récupération<br>",
    "Quel est l'effet de la surcharge progressive dans l'entraînement en musculation ?<br>(a) Diminution de la performance<br>(b) Augmentation de la force et de la taille des muscles<br>(c) Augmentation du risque de blessure<br>",
    "Qu'est-ce que le DOMS ?<br>(a) Douleur et raideur musculaires qui apparaissent plusieurs heures à jours après un exercice inhabituel ou rigoureux<br>(b) Un type de protéine consommée après l'entraînement<br>(c) Un supplément pour améliorer la performance<br>",
    "Quel type de respiration est recommandé pendant les exercices de musculation ?<br>(a) Respiration rapide et superficielle<br>(b) Respiration profonde, inspirer sur l'effort<br>(c) Retenir son souffle pendant toute la durée de l'exercice<br>",
    "Quelle est l'importance du repos dans un programme d'entraînement en musculation ?<br>(a) Il n'est pas important tant que l'alimentation est correcte<br>(b) Il est crucial pour la récupération et la croissance musculaire<br>(c) Il diminue l'efficacité de l'entraînement<br>",
]

reponses = ['b', 'c', 'b', 'b', 'a', 'b', 'b', 'a', 'b', 'b']

# La route principale pour afficher le quiz
@app.route('/', methods=['GET', 'POST'])
def quiz():
    score = 0
    if request.method == 'POST':
        for i, q in enumerate(questions):
            user_answer = request.form.get(str(i))
            if user_answer == reponses[i]:
                score += 1
        return f'Vous avez obtenu {score} sur {len(questions)}.<br><a href="/">Recommencer le quiz</a>'
    # Chargement du template HTML depuis un string (pour simplifier)
    return render_template_string(open('quiz_template.html').read(), questions=questions)

if __name__ == '__main__':
    app.run(debug=True)